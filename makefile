GCC = gcc
SOURCES = $(wildcard *.c)
BINAIRES = $(patsubst %.c,%.o,${SOURCES})


all: main

main: ${BINAIRES}
#${GCC} fonctions.o main.c -o main
	${GCC} $^ -o $@

#fonctions.o: fonctions.c
%.o: %.c fonctions.h
#${GCC} -c fonctions.c
	${GCC} -c $<

clean:
	rm main
	rm *.o
