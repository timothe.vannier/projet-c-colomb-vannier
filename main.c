/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 2                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Analyse adresse IP                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Eliot COLOMB                                                 *
*                                                                             *
*  Nom-prénom2 : Timothé VANNIER                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : main.c                                                    *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "fonctions.h"
#include <math.h>

int main(){
	//variable pour stocker l'adresse ip saisi par l'utilisateur
	char c[45];

	printf("entrez une adresse iP : \n");
	gets(c);

	//verification du format de l'ip
	if (verifierFormat(c)){
		printf("L'adresse ip rentrée est correcte\n");
	}else {
		printf("Adresse ip invalide, veuillez rééssayer\n");
	}

	//récuperation de chaque champs de l'adresse ip + convertion en décimal
	int c1 = convertirEnValeursNumV2(c,1);
	int c2 = convertirEnValeursNumV2(c,2);
	int c3 = convertirEnValeursNumV2(c,3);
	int c4 = convertirEnValeursNumV2(c,4);
	int cm = convertirEnValeursNumV2(c,5);

	// affichage de la classe et de l'adresse reseau
	decoderIP(c,c1,c2,c3,c4);

	calculerAdresse(c, cm);
}
// adresse de test pour crtl+c / crtl+v
// 01010101.00110011.00011100.10101010/10111 @hote
// 01111111.00000000.00000000.00000001/10111 @ping
