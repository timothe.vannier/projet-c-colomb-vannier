/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 2                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Analyse adresse IP                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Eliot COLOMB                                                 *
*                                                                             *
*  Nom-prénom2 : Timothé VANNIER                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : fonctions.c                                               *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "fonctions.h"
#include <math.h>


boolean verifierFormat(char s[45]){

	// definition du type boolean
		typedef enum {FAUX=0, VRAI=1} boolean;

    int erreur = 0;
    int i;
    int compt = 0;
		boolean reponse;

		// parcours l'adresse et verifie le nombre de . et de / , et si elle
		// est composée uniquement de 0 et de 1
    for ( i = 0 ; erreur == 0 && i < strlen(s) -1  ; i++ ){
    	if (s[i]=='.'){
        compt=compt+1;
        i++;
    	}
			if (s[i]=='/'){
        compt=compt+1;
        i++;
    	}

    	if ( s[i] < '0' || s[i] > '1' ){
        erreur = 1;
    	}

		}
		//verification de l'emplacement des points et du /
		if(erreur==0 && compt==4 && s[8] == '.'&& s[17] == '.' && s[26] == '.' && s[35] == '/'){
    	reponse = VRAI;
		}else{
			reponse = FAUX;
		}
		return reponse;
}




//extraireLesChamps et convertirEnValeursNum sont dans un meme programme pour facilité la transmission des variables
// qui posait probleme lorsque ils étaient séparé.

int convertirEnValeursNumV2(char s[45], int n){

	//extraireLesChamps
	int i;
	char ch[8];
	char re[5];

	if (n < 1 || n > 5){
		return 0;
	}
	if (n == 5){
		for (i = 0 ; i < 5 ; i++){
				// printf("indice : %d \n",i+((n-1)*9));
				re[i] = s[i+((n-1)*9)];
		}

	}else{
		for (i = 0 ; i < 8 ; i++){
				// printf("indice, position : %d , %d \n",i+((n-1)*9), i);
			ch[i] = s[i+((n-1)*9)];
				// printf("test, %c\n", ch[i]);
		}
	}

	// convertirEnValeursNum
	int result = 0;
	int e = 0;
	int r = 0;

	if (n == 5){
		//convertion binaire en décimal
		for(int j=strlen(re)-2; j>=0; j--){
			// printf("indice j : %d\n", j);
			if (re[j]== '1'){
				r = 1;
			}else {
				r = 0;
			}
			// printf("r : %d, ch[j] : %c\n", r, ch[j]);
			result = result + r * puissance(2,e);
			e = e + 1;
		}
	}else{
		//concertion binaire en décimal
		for(int j=strlen(ch)-2; j>=0; j--){
			// printf("indice j : %d\n", j);
			if (ch[j]== '1'){
				r = 1;
			}else {
				r = 0;
			}
			// printf("r : %d, ch[j] : %c\n", r, ch[j]);
			result = result + r * puissance(2,e);
			e = e + 1;
		}
	}

	return result;
}

int puissance(int base,int exp){
	int resultat =  1;
	while (exp >= 1){
		resultat *= base;
		exp--;
	}
	return resultat;
}

void decoderIP(char c[45], int c1,int c2 ,int c3,int c4){
char classe[150];

//definition de la classe de l'adresse
	if (c1 >= 0 && c1 <= 126){
		strcpy(classe, "adresse de classe A");
	}else if(c1 >= 128 && c1 <= 191){
		strcpy(classe, "adresse de classe B");
	}else if(c1 >= 192 && c1 <= 223){
		strcpy(classe, "adresse de classe C");
	}else if(c1 >= 224 && c1 <= 239){
		strcpy(classe, "adresse de classe D");
	}else if(c1 >= 240 && c1 <= 255){
		strcpy(classe, "adresse de classe E");
	}

// definition du type de l'adresse (abandonné)

// nous voulions faire des comparaisons de chaine de caracteres avec strcmp
// pour vérifier les adresses de type ping/ broadcast mais nous avons eu
// des problemes avec la variable chaine qui s'initialisait avec 40 char et donc mettait des ? à la fin de la chaine

	// char chaine[35];
	// strncpy(chaine,c,20);
	// printf("chaine : %s\n", chaine);
	// char ping[] = "01111111.00000000.00000000.00000001";
	// printf("ping_ : %s\n", ping );
	//
	// if (strcmp(chaine, ping) == 0){
	// 	strcpy(result, "adresse de ping(localhost)");
	// }
	// printf("taille chaine : %d, taille ping : %d\n", strlen(chaine), strlen(ping));

// probleme pour définir si une adresse est privé ou publique

	// affichage dans la fonctions car problemes avec les types de retour utilisé
	printf("%s\n", classe);

}

void calculerAdresse(char c[45], int cm){

	//calcul de l'adresse réseau
	char result[1023] = "Adresse Réseau : ";
	int p = 0;
	char stk[] = "00000000.00000000.00000000.00000000";

	// ajout des points si besoin
	if (cm > 8 && cm <= 16){
		cm += 1;
	}else if(cm > 16 && cm <= 24){
		cm += 2;
	}else if(cm > 24 && cm <= 32){
		cm += 3;
	}

	strcat(result, strncpy(stk,c, cm));
	// printf("%d\n", cm);
	printf("%s\n", result);

	//calcul de l'adresse hote si besoin (abandonné)
}


// fonction abandonné car trop de probleme à la récupération du type de tableau de caractere

// char* extraireLesChamps(char s[45], int n){
// 	int i;
// 	char ch[9];
// 	if (n < 1 || n > 5){
// 		return "numero invalide";
// 	}
// 	if (n == 5){
// 		for (i = 0 ; i < 5 ; i++){
// 				// printf("indice : %d \n",i+((n-1)*9));
// 				ch[i] = s[i+((n-1)*9)];
// 			}
// 		}else{
// 		for (i = 0 ; i < 8 ; i++){
// 				// printf("indice, position : %d , %d \n",i+((n-1)*9), i);
// 				ch[i] = s[i+((n-1)*9)];
// 				// printf("test, %c\n", ch[i]);
// 		}
// 	}
// 	char* tab = ch;
// 	printf("ch : %s\n", ch);
// 	printf("tab : %s\n", tab);
// 	return tab;
// }


// fonction abandoné car code trop compliqué qui était source de probleme

// int convertirEnValeursNumV1(char* s){
// 	int result = 0;
// 	int i;
// 	int j;
// 	int exp;
// 	printf("%lu\n", strlen(s));
// 	for (i = strlen(s)-2; i >= 0; i--){
// 		printf("indice : %d\n",i);
// 		printf("%c\n",s[i]);
// 		if(s[i] == '1'){
// 			exp=2;
// 			for(j=0;j<strlen(s)-2-i;j++){
// 				exp=exp*2;
// 			}
// 			result = result + exp;
// 			printf("apres calcul : %d\n", result);
// 		}
// 	}
// 	if (s[strlen(s)-1] == '1'){
// 		result = result + 1;
// 	}
// 	printf("test, %d\n",result);
// 	return result;
// }
