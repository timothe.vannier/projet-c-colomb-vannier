/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 2                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Analyse adresse IP                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Eliot COLOMB                                                 *
*                                                                             *
*  Nom-prénom2 : Timothé VANNIER                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : fonctions.h                                               *
*                                                                             *
******************************************************************************/

typedef enum {FAUX=0, VRAI=1} boolean;

boolean verifierFormat(char s[45]);
int convertirEnValeursNumV2(char s[45], int n);
int puissance(int base, int exp);
void decoderIP(char c[45], int c1, int c2, int c3, int c4);
void calculerAdresse(char c[45], int cm);

// fonctions abandonées
// char* extraireLesChamps(char s[45], int n);
// int convertirEnValeursNumV1(char* s);
